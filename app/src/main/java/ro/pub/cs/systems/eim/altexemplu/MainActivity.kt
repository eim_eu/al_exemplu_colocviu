package ro.pub.cs.systems.eim.altexemplu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Space
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.altexemplu.ui.theme.AltExempluTheme
import kotlin.reflect.KProperty

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = CustomViewModel()
        setContent {
            ScreenUI(viewModel = viewModel)
        }
        viewModel.init()
    }
}

@Composable
fun ScreenUI(viewModel: CustomViewModel) {
    val context = LocalContext.current

    val input1 by rememberSaveable {
        viewModel.input1
    }
    val input2 by rememberSaveable {
        viewModel.input2
    }

    val rememberLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val suma = result.data?.getIntExtra("result", 0)
            Toast.makeText(context, "Ok cu suna intoarsa $suma", Toast.LENGTH_SHORT).show()
        }
        if (result.resultCode == Activity.RESULT_CANCELED) {
            val suma = result.data?.getIntExtra("result", 0)
            Toast.makeText(context, "Canceled cu suna intoarsa $suma", Toast.LENGTH_SHORT).show()
        }
    }
    Column {
        Button(
            onClick = {
                // Navigate to secondary activity
                val intent = Intent(context, MainActivity2::class.java)
                intent.putExtra("input1", input1)
                intent.putExtra("input2", input2)
                rememberLauncher.launch(intent)
            },
        ) {
            Text(text = "Navigate to secondary activity")
        }
        Row {
            Text(text = "Count1 : $input1")
            Spacer(modifier = Modifier.width(40.dp))
            Text(text = "Count2: $input2")
        }
        Row {
            Button(onClick = {
                viewModel.incrementInput1()
            }) {
                Text(text = "Press me!")
            }
            Spacer(modifier = Modifier.width(40.dp))
            Button(onClick = {
                viewModel.incrementInput2()
            }) {
                Text(text = "Press me too!")
            }
        }
    }
}

