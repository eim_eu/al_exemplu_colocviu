package ro.pub.cs.systems.eim.altexemplu

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class CustomViewModel {
    val input1 = mutableIntStateOf(0)
    val input2 = mutableIntStateOf(0)
    fun incrementInput1() {
        input1.intValue += 1
    }

    fun incrementInput2() {
        input2.intValue += 1
    }

    fun init() {
        input1.intValue = 0
        input2.intValue = 0
    }

}