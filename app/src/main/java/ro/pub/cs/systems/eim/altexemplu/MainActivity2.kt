package ro.pub.cs.systems.eim.altexemplu

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.altexemplu.ui.theme.AltExempluTheme

class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val input1 = intent.getIntExtra("input1", 0)
            val input2 = intent.getIntExtra("input2", 0)
            val suma = input1 + input2
            Column {
                OutlinedTextField(value = "Rez: $suma", onValueChange = {},
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp))
                Row (modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)){
                    Button(onClick = {
                        setResult(RESULT_OK, intent.putExtra("result", suma))
                        finish() }) {
                        Text(text = "OK")
                    }
                    Spacer(modifier = Modifier.width(16.dp))
                    Button(onClick = {
                        setResult(RESULT_CANCELED, intent.putExtra("result", 0))
                        finish() }) {
                        Text(text = "Cancel")
                    }

                }
            }
        }
    }

}

